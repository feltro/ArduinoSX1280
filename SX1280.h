/**< EBI Interface

written by Luca Feltrin
*/
#ifndef SX1280_H
#define SX1280_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#include <SPI.h>


// Uncomment to enable printing out nice debug messages.
//#define EBI_DEBUG

#define SX1280_NSS	7

#define NOP	0x00

#ifdef ARDUINO_AVR_UNO
	#define DEBUG_PRINTER Serial
#elif ARDUINO_SAMD_ZERO
	#define DEBUG_PRINTER Serial
#elif ARDUINO_SAMD_MKRZERO
	#define DEBUG_PRINTER Serial
#else
	#define DEBUG_PRINTER Serial
#endif


// Define where debug output will be printed.

// Setup debug printing macros.
static void DEBUG_BUFFER_PRINTLN(uint8_t *buffer, int length);
#ifdef EBI_DEBUG
	#define DEBUG_PRINT(...) { DEBUG_PRINTER.print(__VA_ARGS__); }
	#define DEBUG_PRINTLN(...) { DEBUG_PRINTER.println(__VA_ARGS__); }  
#else
	#define DEBUG_PRINT(...) {}
	#define DEBUG_PRINTLN(...) {}
#endif



/**
*	SX1280 Class
*	Transceiver handler
*/
class SX1280 {
	public:
		SX1280();							/**< Creates a handler of the transceiver */
		void begin();							/**< Initializes the SX1280 handler */
	protected:
		void writeSpiRegister(uint16_t address, uint8_t buffer[], int length);	/**< writes a portion of memory of the SX1280 through SPI \param address starting address \param buffer data to transfer \param length length of the data to transfer in bytes */
		void readSpiRegister(uint16_t address, uint8_t *buffer[], int length);	/**< reads a portion of memory of the SX1280 through SPI \param address starting address \param buffer destination buffer for the data \param length length of the data to transfer in bytes */
};

#endif
