// Example testing sketch for SX1280 Semtech Evaluation Board
// Written by Luca Feltrin, University of Bologna, public domain

#include "SX1280.h"

SX1280 hSX1280;

void setup() {
  Serial.begin(115200);     //Debug
  Serial.println("Starting...");
  hSX1280.begin();
}

void loop() {
  delay(10000);
  Serial.println("I'm doing nothing!");
}