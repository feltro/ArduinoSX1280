/* SX1280 Interface

written by Luca Feltrin
*/

#include "SX1280.h"
#include <SPI.h>


SX1280::SX1280() {
}

void SX1280::begin() {
	pinMode(SX1280_NSS, OUTPUT);
	SPI.begin();
}

void SX1280::writeSpiRegister(uint16_t address, uint8_t buffer[], int length) {
	uint8_t status;
	digitalWrite(SX1280_NSS, LOW);
	
	status = SPI.transfer(0x18);
	status = SPI.transfer((address >> 8) & 0xFF);
	status = SPI.transfer((address >> 0) & 0xFF);
	
	for(int i=0;i<length;i++) status = SPI.transfer(buffer[i]);
  
	digitalWrite(SX1280_NSS, HIGH);
}

void SX1280::readSpiRegister(uint16_t address, uint8_t *buffer[], int length) {
	uint8_t status;
	digitalWrite(SX1280_NSS, LOW);
	
	status = SPI.transfer(0x19);
	status = SPI.transfer((address >> 8) & 0xFF);
	status = SPI.transfer((address >> 0) & 0xFF);
	
	status = SPI.transfer(NOP);
	
	for(int i=0;i<length;i++) *buffer[i] = SPI.transfer(NOP);
  
	digitalWrite(SX1280_NSS, HIGH);
}

#ifdef EBI_DEBUG
  static void DEBUG_BUFFER_PRINTLN(uint8_t *buffer, int length){
	  for(int i=0;i<length-1;i++) {
		  if(buffer[i]<16) DEBUG_PRINT("0")
		  DEBUG_PRINT(buffer[i], HEX)
		  DEBUG_PRINT(" ")
	  }
	  if(buffer[length-1]<16) DEBUG_PRINT("0")
	  DEBUG_PRINTLN(buffer[length-1], HEX)		  
  }
#else
  static void DEBUG_BUFFER_PRINTLN(uint8_t *buffer, int length){}
#endif
